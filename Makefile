# gentoo-tools
# See LICENSE file for copyright and license details.

include config.mk

all:

dist:
	@echo creating dist tarball
	@mkdir -p gentoo-tools-${VERSION}
	@cp -R LICENSE Makefile config.mk bin gentoo-tools-${VERSION}
	@tar -cf gentoo-tools-${VERSION}.tar gentoo-tools-${VERSION}
	@gzip gentoo-tools-${VERSION}.tar
	@rm -rf gentoo-tools-${VERSION}

install: all
	@echo installing scripts to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cd bin; \
	for i in *; \
	do \
		cp $$i ${DESTDIR}${PREFIX}/bin; \
		chmod 755 ${DESTDIR}${PREFIX}/bin/$$i; \
	done; true
	@echo installing manual page to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed "s/VERSION/${VERSION}/g" < gt.1 > ${DESTDIR}${MANPREFIX}/man1/gt.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/gt.1

uninstall:
	@echo removing scripts from ${DESTDIR}${PREFIX}/bin
	@cd bin; \
	for i in *; \
	do \
		rm -f ${DESTDIR}${PREFIX}/bin/$$i; \
	done; true

.PHONY: all dist install uninstall
